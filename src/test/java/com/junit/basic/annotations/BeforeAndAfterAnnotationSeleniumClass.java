package com.junit.basic.annotations;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BeforeAndAfterAnnotationSeleniumClass {

	private WebDriver driver;

	@Before
	public void setUp(){
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
	}

	@After
	public void tearDown(){
		driver.quit();
	}

	@Test
	public void openGoogleChromeGoogleDotCom(){
		driver.get("http://google.com");
	}

	@Test
	public void openGoogleChromeYahooDotCom(){
		driver.get("http://yahoo.com");
	}

}