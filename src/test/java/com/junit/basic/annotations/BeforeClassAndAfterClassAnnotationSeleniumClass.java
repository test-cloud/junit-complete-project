package com.junit.basic.annotations;

import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BeforeClassAndAfterClassAnnotationSeleniumClass {

    public static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

    @Test
    public void openGoogleChromeGoogleDotCom() {
        driver.get("http://google.com");
    }

    @Test
    public void openGoogleChromeYahooDotCom() {
        driver.get("http://yahoo.com");
    }

}