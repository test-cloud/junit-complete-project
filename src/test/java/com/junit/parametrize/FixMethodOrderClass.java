package com.junit.parametrize;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.JVM)
public class FixMethodOrderClass {

    @Test
    public void AtestMethodOne() {
        System.out.println("Inside testMethodOne");
    }

    @Test
    public void BtestMethodTwo() {
        System.out.println("Inside testMethodTwo");
    }

    @Test
    public void CtestMethodThree() {
        System.out.println("Inside testMethodThree");
    }

    @Test
    public void DtestMethodFour() {
        System.out.println("Inside testMethodFour");
    }

    @Test
    public void EtestMethodFive() {
        System.out.println("Inside testMethodFive");
    }

    @Test
    public void FtestMethodSix() {
        System.out.println("Inside testMethodSix");
    }

    @Test
    public void GtestMethodSeven() {
        System.out.println("Inside testMethodSeven");
    }
}